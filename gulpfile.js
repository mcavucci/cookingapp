'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function () {
  gulp.src('./app/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./app/css'));
});
 
gulp.task('sass:watch', function () {
  //gulp.watch('*.scss', ['sass']);
  gulp.watch('./app/**/*.scss', ['sass'])
});

gulp.task('default', ['sass:watch']);