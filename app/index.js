var React = require('react');
var App = require('./app.jsx');
var data = require('api/recipe.json');

React.render(
  <App data={data} />,
  document.getElementById('app')
);
