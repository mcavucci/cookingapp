var React = require('react');
var recipeData = require('api/recipe.json');
var Masthead = require('components/masthead/masthead.jsx');
var FeaturedPanel = require('components/featured-panel/featured-panel.jsx');
var Testimonials = require('components/testimonials/testimonials.jsx');
var GenericPanel = require('components/generic-panel/generic-panel.jsx');
var Ingredients = require('components/ingredients/ingredients.jsx');
var GenericImage = require('components/generic-image/generic-image.jsx');
var Stages = require('components/stages/stages.jsx');
var FinalStage = require('components/final-stage/final-stage.jsx');
var SharingSection = require('components/sharing-section/sharing-section.jsx');
var Controller = require('components/controller/controller.jsx');
var Navigator = require('components/navigator/navigator.jsx');
var Footer = require('components/footer/footer.jsx');
var VoiceIndicator = require('components/voice-indicator/voice-indicator.jsx');

var getAccumulatedArray = require('utils/accumulate.js');

React.initializeTouchEvents(true);

var App = React.createClass({
  getInitialState() {
    return {
      isPlaying: false,
      currentTime: 0,
      currentStage: 0,
      commandStatus: 'ready',
      recipe: recipeData
    }
  },

  getStageDurations() {
    return this.state.recipe.stages.map(stage => {
      return stage.duration;
    })
  },

  getAccumulatedDurations() {
    var stageDurations = this.getStageDurations();
    return getAccumulatedArray(stageDurations);
  },

  updateCurrentStage() {
    var durations = this.getAccumulatedDurations();
    var currentDuration = durations[this.state.currentStage];

    if (this.state.currentTime > currentDuration) {
      this.setState({
        currentStage: this.state.currentStage + 1
      });
    }
  },

  updateCurrentTime() {
    this.setState({
      currentTime: this.state.currentTime + 1
    });
  },

  incrementTimer() {
    this.updateCurrentTime();
    this.updateCurrentStage();
  },

  togglePlay() {
    this.setState({
      isPlaying: !this.state.isPlaying
    });
  },

  handleTimer() {
    if(this.state.isPlaying) {
      this.timer = setInterval(this.incrementTimer, 1000);
    } else {
      clearInterval(this.timer);
    }
  },

  handleToggle(event) {
    if(event) {
      event.preventDefault();
    }

    this.togglePlay();
    this.forceUpdate(this.handleTimer);
  },

  onNavigatorClick(event) {
    event.preventDefault();
    var customTime = parseInt(event.currentTarget.dataset.duration);

    if(customTime && this.state.isPlaying) {
      this.setState({
        currentTime: customTime
      });
    }
  },

  onCommandMismatch(possiblePhrases) {
    this.setState({
      commandStatus: 'error'
    });
  },

  registerVoiceCommands() {
    this.commands = {
      'go': this.handleToggle,
      'stop': this.handleToggle,
      //'go': this.handleToggle,
      'next': (() => {
        this.setState({
          currentStage: currentStage + 1
        })
      }),
      'previous': (() => {
        this.setState({
          currentStage: currentStage - 1
        })
      })
    }

    if(annyang) {
      annyang.debug(true);
      annyang.setLanguage('en-US');
      annyang.addCallback('resultNoMatch', this.onCommandMismatch);
      annyang.addCallback('resultMatch', () => {
        this.setState({
          commandStatus: 'success'
        });
      });
      annyang.addCommands(this.commands);
      annyang.start();
    }
  },

  componentDidMount() {
    this.registerVoiceCommands();
  },

  render() {
    var controllerText = this.state.isPlaying ? 'Pause Live Cooking!' : 'Tap & Go Live-Cooking!';
    var durations = this.getAccumulatedDurations();

    console.log(this.state)

    return (
      <main className="main">
        <Masthead />
        <Navigator {...this.state} durations={durations} setCustomTime={this.onNavigatorClick} />
        <FeaturedPanel />
        <Testimonials icon="chat" />
        <GenericPanel title="Ingredients" subtitle="All you need to start cooking this recipe!" icon="alarm_clock" />
        <Ingredients {...this.state} />
        <GenericImage src={this.state.recipe.main_image} alt="Recipe" />
        <GenericPanel title="Preparation" subtitle="Follow the steps if you want to lick your fingers!"/>
        <Controller {...this.state} controllerText={controllerText} callback={this.handleToggle} />
        <Stages {...this.state} accumDurations={durations} durations={this.getStageDurations()} />
        <GenericPanel title="Pass it on" subtitle="Share this recipe and spread the love"/>
        <SharingSection icon="heart" />
        <Footer />
        <VoiceIndicator status={this.state.commandStatus} />
      </main>
    );
  }
});

module.exports = App;
