
var gulp = require('gulp');
var sass = require('gulp-sass');
var plugins = require('gulp-load-plugins')();

gulp.task('styles', function() {
  gulp.src('_assets/stylesheets/scss/main.scss')
      .pipe(sass({outputStyle: 'compressed'}))
      .pipe(plugins.autoprefixer(autoprefixerOptions))
      .pipe(gulp.dest('_assets/stylesheets/css/'));
});

var autoprefixerOptions = {
  browsers: ['last 2 versions', 'IE 9', '> 5%', 'Firefox ESR']
};

//Watch task
gulp.task('default',function() {
    gulp.watch('_assets/stylesheets/scss/**/*', ['styles']);
});
