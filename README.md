# React Playbook

To Begin, simply run: 

```sh
$ npm install
```

or if you do not have file permissions, run: 

```sh
$ sudo npm install
```

To run the server, run: 

```sh
$ npm start
```

And to compile Sass, run: 

```sh
$ npm run sass-build
```

Playbook is currently using Webpack, for further information about webpack, visit [Webpack's documentation]

[Webpack's documentation]: http://webpack.github.io/
